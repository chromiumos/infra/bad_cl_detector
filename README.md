# Bad CL Detector [Discontinued]

[TOC]

https://chromium.googlesource.com/chromiumos/infra/bad_cl_detector

## Deprecated [Dec 2018]
As of Dec 2018, the underlying database calls in this repository are
no longer supported in Chromite.

As of Sep 2020 the code in this repo was deleted because it contains
uninclusive language. See go/chromium-project-code-inclusion and
crbug.com/1119243. The repo is left unarchived because some branched
manifests reference it.

## Purpose:

A system to detect if a given build failure is caused by bad CL.

## Getting started

The virtualenv for this project is managed with pipenv. If you don't already
have pipenv installed, you can install it with "pip install pipenv".

Then run:

```bash
deactivate  # make sure you are not already in a virtualenv
pipenv --two install
protoc -I=lib/protos --python_out=lib/protos
```

You can fetch cidb credentials with "cros cidbcreds". You will need to pass in
the db credentials' path to the script.

## Training & cross validation

Downloading training data, training a model and cross validating is done by one
script:

```bash
pipenv run python lib/bad_cl_detector_driver.py --cred-dir=$CRED_DIR
```

Or, you can enter a virtualenv shell with "pipenv shell", and then just run
python lib/<etc>.

Currently this requires word2vec and word2int files to work. Making this
self-contained is a work in progress.

## Tests

Tests are run with pytest:

```bash
pipenv run pytest
```


